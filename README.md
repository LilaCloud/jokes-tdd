# JOKER

A Flutter app telling (bad) jokes.

## What's this about?

This sample project uses BLoC architecture and is developed following TDD principles.

Well, mostly implemented using TDD: When working on Favorites I ran out of time and decided to write no more tests, because I already wrote more than 30, but was requested to just write at least one ;)