import 'dart:ui';

import 'package:flutter/material.dart';

class Dimensions {
  static const double SPACE_LARGE = 18.0;
  static const double SPACE_DEFAULT = 12.0;
  static const double SPACE_SMALL = 6.0;
  static const double SPACE_TINY = 3.0;

  static const double RADIUS = 5.0;

  static const double MAX_WIDTH_CONTENT = 720.0;
  static const double MAX_WIDTH_CONTENT_THRESHOLD = 750.0;
  static const double BIG_SCREEN_THRESHOLD = 900.0;

  static const double BORDER_DEFAULT = 1.0;
  static const double BORDER_HIGHLIGHT = 2.0;
}

class CustomColors {
  static const Color GREEN_DARK = Color.fromRGBO(104, 159, 56, 1.0);
  static const Color GREEN_DARK_TRANSLUCENT = Color.fromRGBO(104, 159, 56, 0.6);
  static const Color BORDER = Color.fromRGBO(189, 189, 189, 1.0);
}

bool isWideScreen(BuildContext context) => MediaQuery.of(context).size.width > Dimensions.MAX_WIDTH_CONTENT_THRESHOLD;
