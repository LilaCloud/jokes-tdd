import 'package:logger/logger.dart';

Logger getLogger() {
  return Logger(printer: PrettyPrinter(methodCount: 0, colors: false, printTime: true));
}
