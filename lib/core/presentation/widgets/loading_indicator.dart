import 'package:flutter/material.dart';
import 'package:jokes/core/util/layout.dart';

class LoadingIndicatorWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
   return const Center(child: Padding(
     padding: EdgeInsets.all(Dimensions.SPACE_DEFAULT),
     child: CircularProgressIndicator(),
   ));
  }
}