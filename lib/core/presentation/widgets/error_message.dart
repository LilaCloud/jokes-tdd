import 'package:flutter/material.dart';
import 'package:jokes/core/util/layout.dart';

class ErrorMessageWidget extends StatelessWidget {
  final String message;

  const ErrorMessageWidget({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Dimensions.SPACE_DEFAULT),
      child: Text(
        message,
        style: Theme.of(context).textTheme.headline6.copyWith(color: Colors.red),
        textAlign: TextAlign.center,
      ),
    );
  }
}
