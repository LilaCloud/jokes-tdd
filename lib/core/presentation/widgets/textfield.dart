import 'package:flutter/material.dart';
import 'package:jokes/core/util/layout.dart';
import 'package:meta/meta.dart';

class JokerTextField extends StatelessWidget {
  final TextEditingController controller;
  final ValueChanged<String> onSubmittedListener;
  final String hintLabel;

  const JokerTextField(
      {Key key,
      @required this.controller,
      @required this.onSubmittedListener,
      @required this.hintLabel,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(Dimensions.SPACE_SMALL),
        labelText: hintLabel,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(Dimensions.RADIUS),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: CustomColors.BORDER),
          borderRadius: BorderRadius.circular(Dimensions.RADIUS),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide:  const BorderSide(color: CustomColors.BORDER),
          borderRadius: BorderRadius.circular(Dimensions.RADIUS),
        ),
      ),
      keyboardType: TextInputType.text,
      maxLines: 1,
      onSubmitted: onSubmittedListener,
      controller: controller,
      autofocus: false,
      textAlign: TextAlign.center,
    );
  }
}
