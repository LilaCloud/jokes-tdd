import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:jokes/core/util/layout.dart';

class PrimaryButton extends StatelessWidget {
  final String _label;
  final VoidCallback _onTap;

  const PrimaryButton(this._label, this._onTap);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: CustomColors.GREEN_DARK,
      disabledColor: CustomColors.GREEN_DARK_TRANSLUCENT,
      textColor: Colors.white,
      disabledTextColor: Colors.black54,
      onPressed: _onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: Dimensions.SPACE_SMALL),
        child: Text(_label),
      ),
    );
  }
}

class SecondaryButton extends PrimaryButton {
  const SecondaryButton(String label, VoidCallback onTap) : super(label, onTap);

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      textColor: CustomColors.GREEN_DARK,
      disabledTextColor: CustomColors.GREEN_DARK_TRANSLUCENT,
      borderSide: const BorderSide(color: CustomColors.GREEN_DARK),
      disabledBorderColor: CustomColors.GREEN_DARK_TRANSLUCENT,
      onPressed: _onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: Dimensions.SPACE_SMALL),
        child: Text(_label),
      ),
    );
  }
}
