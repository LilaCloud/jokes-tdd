import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jokes/core/util/layout.dart';
import 'package:jokes/features/jokes/presentation/pages/joke_page.dart';
import 'package:jokes/injection_container.dart';
import 'features/jokes/presentation/bloc/jokes_bloc.dart';
import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<JokesBloc>(
      create: (_) => sl<JokesBloc>(),
      child: MaterialApp(
        title: 'Joker',
        home: JokePage(),
        builder: (BuildContext context, Widget child) {
          final TextTheme textTheme = _getTextTheme(context);
          return Theme(
            data: ThemeData(
                primarySwatch: Colors.deepOrange,
                accentColor: Colors.lightGreen,
                cursorColor: Colors.lightGreen,
                indicatorColor: Colors.lightGreen,
                textTheme: textTheme,
                primaryTextTheme: textTheme,
                accentTextTheme: textTheme,
                inputDecorationTheme:
                    const InputDecorationTheme(labelStyle: TextStyle(color: Colors.black54, fontSize: 14))),
            child: child,
          );
        },
      ),
    );
  }

  TextTheme _getTextTheme(BuildContext context) {
    final bool bigScreen = MediaQuery.of(context).size.width > Dimensions.BIG_SCREEN_THRESHOLD ||
        MediaQuery.of(context).size.height > Dimensions.BIG_SCREEN_THRESHOLD;
    return GoogleFonts.rosarioTextTheme(Theme.of(context).textTheme.copyWith(
          headline1: const TextStyle(fontSize: 72),
          headline5: TextStyle(fontSize: bigScreen ? 32 : 28),
          headline6: TextStyle(fontSize: bigScreen ? 28 : 24),
          subtitle1: TextStyle(fontSize: bigScreen ? 22 : 18),
          subtitle2: TextStyle(fontSize: bigScreen ? 20 : 16),
          bodyText1: TextStyle(fontSize: bigScreen ? 22 : 18),
          bodyText2: TextStyle(fontSize: bigScreen ? 20 : 16),
          button: TextStyle(fontSize: bigScreen ? 20 : 16),
          caption: TextStyle(fontSize: bigScreen ? 18 : 14),
          overline: TextStyle(fontSize: bigScreen ? 16 : 12),
        ));
  }
}
