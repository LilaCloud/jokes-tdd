import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';
import 'package:meta/meta.dart';

class UseCaseRemoveFavorite implements UseCase<List<Joke>, RemoveFavoriteParams> {
  final JokesRepository repository;

  UseCaseRemoveFavorite(this.repository);

  @override
  Future<Either<Failure, List<Joke>>> call(RemoveFavoriteParams params) async {
    return await repository.removeFavorite(params.joke);
  }
}

class RemoveFavoriteParams extends Equatable {
  final Joke joke;

  const RemoveFavoriteParams({@required this.joke});

  @override
  List<Object> get props => <dynamic>[joke];
}
