import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';
import 'package:meta/meta.dart';

class UseCaseAddFavorite implements UseCase<List<Joke>, AddFavoriteParams> {
  final JokesRepository repository;

  UseCaseAddFavorite(this.repository);

  @override
  Future<Either<Failure, List<Joke>>> call(AddFavoriteParams params) async {
    return await repository.addFavorite(params.joke);
  }
}

class AddFavoriteParams extends Equatable {
  final Joke joke;

  const AddFavoriteParams({@required this.joke});

  @override
  List<Object> get props => <dynamic>[joke];
}
