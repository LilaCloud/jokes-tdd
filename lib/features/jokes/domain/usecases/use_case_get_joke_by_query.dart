import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';
import 'package:meta/meta.dart';

class UseCaseGetJokeForQuery implements UseCase<Joke, GetJokeForQueryParams> {
  final JokesRepository repository;

  UseCaseGetJokeForQuery(this.repository);

  @override
  Future<Either<Failure, Joke>> call(GetJokeForQueryParams params) async {
    return await repository.getJokeForQuery(params.query);
  }
}

class GetJokeForQueryParams extends Equatable {
  final String query;

  const GetJokeForQueryParams({@required this.query});

  @override
  List<Object> get props => <dynamic>[query];
}
