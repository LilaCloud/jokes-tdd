import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';

class UseCaseGetFavorites implements UseCase<List<Joke>, NoParams> {
  final JokesRepository repository;

  UseCaseGetFavorites(this.repository);

  @override
  Future<Either<Failure, List<Joke>>> call(NoParams params) async {
    return await repository.getFavorites();
  }
}
