import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';

class UseCaseGetRandomJoke implements UseCase<Joke, NoParams> {
  final JokesRepository repository;

  UseCaseGetRandomJoke(this.repository);

  @override
  Future<Either<Failure, Joke>> call(NoParams params) async {
    return await repository.getRandomJoke();
  }
}
