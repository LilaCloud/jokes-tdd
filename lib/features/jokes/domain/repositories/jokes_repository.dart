import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';

abstract class JokesRepository {
  Future<Either<Failure, Joke>> getJokeForQuery(String query);

  Future<Either<Failure, Joke>> getRandomJoke();

  Future<Either<Failure, bool>> isFavorite(Joke joke);

  Future<Either<Failure, List<Joke>>> getFavorites();

  Future<Either<Failure, List<Joke>>> addFavorite(Joke joke);

  Future<Either<Failure, List<Joke>>> removeFavorite(Joke joke);
}
