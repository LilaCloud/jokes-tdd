import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class Joke extends Equatable {

  final int id;
  final bool isTwoPart;
  final String text1;
  final String text2;
  final bool isFavorite;

  const Joke({@required this.id, @required this.isTwoPart, @required this.text1, this.text2, this.isFavorite});

  @override
  List<Object> get props => <dynamic>[id, isTwoPart, text1, text2, isFavorite];
}
