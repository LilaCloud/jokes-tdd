import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jokes/core/presentation/widgets/error_message.dart';
import 'package:jokes/core/presentation/widgets/loading_indicator.dart';
import 'package:jokes/core/util/layout.dart';
import 'package:jokes/core/util/logger.dart';
import 'package:jokes/features/jokes/presentation/bloc/jokes_bloc.dart';
import 'package:jokes/features/jokes/presentation/bloc/jokes_state.dart';
import 'package:jokes/features/jokes/presentation/widgets/joke_display.dart';

class FavoritesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Favorites')),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: Dimensions.SPACE_DEFAULT),
      child: BlocBuilder<JokesBloc, JokesState>(builder: (BuildContext context, JokesState state) {
        getLogger().d('BLoC State: $state');
        if (state is JokesStateEmpty) {
          return Text('Unexpected error', style: Theme.of(context).textTheme.headline6);
        }
        if (state is JokesStateLoading) {
          return LoadingIndicatorWidget();
        }
        if (state is JokesStateLoaded) {
          return ListView.builder(
              itemCount: state.favorites.length,
              itemBuilder: (BuildContext context, int index) => JokeDisplayWidget(joke: state.favorites[index]));
        }
        if (state is JokesStateError) {
          return ErrorMessageWidget(message: state.message);
        }
        return Container();
      }),
    );
  }
}
