import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jokes/core/presentation/widgets/buttons.dart';
import 'package:jokes/core/presentation/widgets/error_message.dart';
import 'package:jokes/core/presentation/widgets/loading_indicator.dart';
import 'package:jokes/core/presentation/widgets/textfield.dart';
import 'package:jokes/core/util/layout.dart';
import 'package:jokes/core/util/logger.dart';
import 'package:jokes/features/jokes/presentation/bloc/bloc.dart';
import 'package:jokes/features/jokes/presentation/bloc/jokes_bloc.dart';
import 'package:jokes/features/jokes/presentation/pages/favorites_page.dart';
import 'package:jokes/features/jokes/presentation/widgets/joke_display.dart';

class JokePage extends StatefulWidget {
  @override
  _JokePageState createState() => _JokePageState();
}

class _JokePageState extends State<JokePage> {
  final TextEditingController controller = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Joker')),
      drawer: _buildDrawer(context),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Dimensions.SPACE_DEFAULT),
      child: ListView(
        children: <Widget>[_buildJokeDisplay(context), _buildJokeControls(context)],
      ),
    );
  }

  Widget _buildJokeDisplay(BuildContext context) {
    return BlocBuilder<JokesBloc, JokesState>(builder: (BuildContext context, JokesState state) {
      getLogger().d('BLoC State: $state');
      if (state is JokesStateEmpty) {
        return Text(
          'Click buttons below to either load a random joke or a joke containing the search query.',
          style: Theme.of(context).textTheme.headline6,
        );
      }
      if (state is JokesStateLoading) {
        return LoadingIndicatorWidget();
      }
      if (state is JokesStateLoaded) {
        return JokeDisplayWidget(joke: state.joke);
      }
      if (state is JokesStateError) {
        return ErrorMessageWidget(message: state.message);
      }
      return Container();
    });
  }

  Widget _buildJokeControls(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: Dimensions.SPACE_LARGE),
      child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(Dimensions.SPACE_SMALL),
                child: PrimaryButton(
                    'Random Joke', () => BlocProvider.of<JokesBloc>(context).add(JokesEventGetRandomJoke())),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(Dimensions.SPACE_SMALL),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: Dimensions.SPACE_TINY),
                      child: JokerTextField(
                          controller: controller,
                          hintLabel: 'Search Query',
                          onSubmittedListener: (String query) {
                            // nothing to do here
                          }),
                    ),
                    PrimaryButton('Query Joke', () {
                      BlocProvider.of<JokesBloc>(context).add(JokesEventGetJokeForQuery(controller.value.text));
                      controller.clear();
                    }),
                  ],
                ),
              ),
            ),
          ]),
    );
  }

  Widget _buildDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const DrawerHeader(
            child: FlutterLogo(),
            decoration: BoxDecoration(color: Colors.grey),
          ),
          ListTile(
            trailing: const Icon(Icons.favorite),
            title: const Text('Favorites'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute<void>(builder: (BuildContext context) => FavoritesPage()));
            },
          ),
          ListTile(
            trailing: const Icon(Icons.info),
            title: const Text('About'),
            onTap: () {
              Navigator.pop(context);
              showAboutDialog(
                  context: context,
                  applicationName: 'Joker',
                  applicationVersion: '0.0.1',
                  applicationLegalese:
                      'Legal Information\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
            },
          )
        ],
      ),
    );
  }
}
