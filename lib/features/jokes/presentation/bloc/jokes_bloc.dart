import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/core/util/logger.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_add_favorite.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_favorites.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_joke_by_query.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_random_joke.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_remove_favorite.dart';
import 'package:jokes/features/jokes/presentation/bloc/bloc.dart';
import 'package:meta/meta.dart';

const String STORAGE_FAILURE_MESSAGE = 'An error occurred while accessing local storage.';
const String SERVER_FAILURE_MESSAGE =
    'Please check your internet connection. Either you are not connected, or the server did not answer successfully.';

class JokesBloc extends Bloc<JokesEvent, JokesState> {
  final UseCaseGetRandomJoke getRandomJoke;
  final UseCaseGetJokeForQuery getJokeForQuery;
  final UseCaseGetFavorites getFavorites;
  final UseCaseAddFavorite addFavorite;
  final UseCaseRemoveFavorite removeFavorite;

  Joke lastJoke;
  List<Joke> lastFavorites;

  JokesBloc(
      {@required this.getRandomJoke,
      @required this.getJokeForQuery,
      @required this.getFavorites,
      @required this.addFavorite,
      @required this.removeFavorite}) {
    // trigger loading data, when creating the bloc instance, so that the pages already have some initial data when opened
    add(JokesEventGetFavorites());
    add(JokesEventGetRandomJoke());
  }

  @override
  JokesState get initialState => JokesStateEmpty();

  @override
  Stream<JokesState> mapEventToState(
    JokesEvent event,
  ) async* {
    if (event is JokesEventGetRandomJoke) {
      yield JokesStateLoading();
      yield* _eitherLoadedJokeOrErrorState(await getRandomJoke(NoParams()));
    } else if (event is JokesEventGetJokeForQuery) {
      yield JokesStateLoading();
      yield* _eitherLoadedJokeOrErrorState(await getJokeForQuery(GetJokeForQueryParams(query: event.query)));
    } else if (event is JokesEventGetFavorites) {
      yield JokesStateLoading();
      yield* _eitherLoadedFavoritesOrErrorState(await getFavorites(NoParams()));
    } else if (event is JokesEventAddFavorite) {
      yield JokesStateLoading();
      yield* _eitherLoadedFavoritesOrErrorState(await addFavorite(AddFavoriteParams(joke: event.joke)));
    } else if (event is JokesEventRemoveFavorite) {
      yield JokesStateLoading();
      yield* _eitherLoadedFavoritesOrErrorState(await removeFavorite(RemoveFavoriteParams(joke: event.joke)));
    } else {
      getLogger().e('Event not handled: $event!');
    }
  }

  Stream<JokesState> _eitherLoadedJokeOrErrorState(
    Either<Failure, Joke> failureOrJoke,
  ) async* {
    yield failureOrJoke.fold(
      (Failure failure) => JokesStateError(message: _mapFailureToMessage(failure)),
      (Joke joke) {
        if (lastFavorites != null && lastFavorites.indexWhere((Joke element) => element.id == joke.id) > -1) {
          // TODO: [CLEAN]
          // This is unclean, because I'm using a model class in the presentation layer.
          // I'm doing this because all fields are final in oder to be able to use Equatable.
          // So I need to create a new instance to change the favorite flag.
          // A much cleaner solution would be to let the repository store the last joke and the last favorites and
          // always return both. This way the repository is the single source of truth and we would have no logic like this in the bloc.
          // But this is just taking uo too much time for now.
          joke =
              JokeModel(id: joke.id, isFavorite: true, isTwoPart: joke.isTwoPart, text1: joke.text1, text2: joke.text2);
        }
        lastJoke = joke;
        return JokesStateLoaded(joke, lastFavorites);
      },
    );
  }

  Stream<JokesState> _eitherLoadedFavoritesOrErrorState(
    Either<Failure, List<Joke>> failureOrJoke,
  ) async* {
    yield failureOrJoke.fold(
      (Failure failure) => JokesStateError(message: _mapFailureToMessage(failure)),
      (List<Joke> favorites) {
        lastFavorites = favorites;
        if (lastJoke != null && lastFavorites.indexWhere((Joke element) => element.id == lastJoke.id) == -1) {
          // This is a little unclean, because I'm using a model class in the presentation layer.
          // I'm doing this because all fields are final in oder to be able to use Equatable.
          // So I need to create a new instance to change the favorite flag.
          // It would be cleaner to create separate models for the different layers and convert those
          lastJoke = JokeModel(
              id: lastJoke.id,
              isFavorite: false,
              isTwoPart: lastJoke.isTwoPart,
              text1: lastJoke.text1,
              text2: lastJoke.text2);
        }
        return JokesStateLoaded(lastJoke, favorites);
      },
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case StorageFailure:
        return STORAGE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
