import 'package:equatable/equatable.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:meta/meta.dart';

@immutable
abstract class JokesEvent extends Equatable {
  @override
  List<Object> get props => <dynamic>[];
}

class JokesEventGetJokeForQuery extends JokesEvent {
  final String query;

  JokesEventGetJokeForQuery(this.query);

  @override
  List<Object> get props => <dynamic>[query];
}

class JokesEventGetRandomJoke extends JokesEvent {
  @override
  List<Object> get props => <dynamic>[];
}

class JokesEventGetFavorites extends JokesEvent {
  @override
  List<Object> get props => <dynamic>[];
}

class JokesEventAddFavorite extends JokesEvent {
  final Joke joke;

  JokesEventAddFavorite(this.joke);

  @override
  List<Object> get props => <dynamic>[joke];
}

class JokesEventRemoveFavorite extends JokesEvent {
  final Joke joke;

  JokesEventRemoveFavorite(this.joke);

  @override
  List<Object> get props => <dynamic>[joke];
}
