import 'package:equatable/equatable.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:meta/meta.dart';

@immutable
abstract class JokesState extends Equatable {
  @override
  List<Object> get props => <dynamic>[];
}

class JokesStateEmpty extends JokesState {
  @override
  List<Object> get props => <dynamic>[];
}

class JokesStateLoading extends JokesState {
  @override
  List<Object> get props => <dynamic>[];
}

class JokesStateLoaded extends JokesState {

  final Joke joke;

  final List<Joke> favorites;

  JokesStateLoaded(this.joke, this.favorites);

  @override
  List<Object> get props => <dynamic>[joke, favorites];
}

class JokesStateError extends JokesState {
  final String message;

  JokesStateError({@required this.message});

  @override
  List<Object> get props => <dynamic>[message];
}


