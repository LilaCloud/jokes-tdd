import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jokes/core/util/layout.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/presentation/bloc/bloc.dart';

class JokeDisplayWidget extends StatelessWidget {
  final Joke joke;

  const JokeDisplayWidget({Key key, this.joke}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: isWideScreen(context)
            ? const BoxConstraints(maxWidth: Dimensions.MAX_WIDTH_CONTENT)
            : const BoxConstraints(),
        child: Padding(
          padding: const EdgeInsets.only(
              top: 30, bottom: Dimensions.SPACE_LARGE, left: Dimensions.SPACE_SMALL, right: Dimensions.SPACE_SMALL),
          child: Card(
            shape: RoundedRectangleBorder(
                side: const BorderSide(), borderRadius: BorderRadius.circular(Dimensions.RADIUS)),
            child: Padding(
              padding: const EdgeInsets.all(Dimensions.SPACE_DEFAULT),
              child: _buildJokeContent(context, joke),
            ),
          ),
        ));
  }

  Widget _buildJokeContent(BuildContext context, Joke joke) {
    return Column(
      children: <Widget>[
        _buildFavoriteIcon(context),
        Text(joke.text1, textAlign: TextAlign.center),
        if (joke.isTwoPart)
          const Divider(
            height: 80,
            thickness: Dimensions.BORDER_HIGHLIGHT,
            indent: Dimensions.SPACE_LARGE,
            endIndent: Dimensions.SPACE_LARGE,
          )
        else
          Container(),
        if (joke.isTwoPart) Text(joke.text2, textAlign: TextAlign.center) else Container(),
      ],
    );
  }

  Widget _buildFavoriteIcon(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: Dimensions.SPACE_DEFAULT),
      child: Align(
        alignment: Alignment.topRight,
        child: IconButton(
            icon: Icon(
              joke.isFavorite ? Icons.favorite : Icons.favorite_border,
              color: CustomColors.GREEN_DARK,
            ),
            onPressed: () {
              if (joke.isFavorite)
                BlocProvider.of<JokesBloc>(context).add(JokesEventRemoveFavorite(joke));
              else
                BlocProvider.of<JokesBloc>(context).add(JokesEventAddFavorite(joke));
            }),
      ),
    );
  }
}
