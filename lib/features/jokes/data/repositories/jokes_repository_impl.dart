import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/util/logger.dart';
import 'package:jokes/features/jokes/data/datasources/jokes_local_data_source.dart';
import 'package:jokes/features/jokes/data/datasources/jokes_remote_data_source.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';

typedef _QueryOrRandomChooser = Future<Joke> Function();

class JokesRepositoryImpl implements JokesRepository {
  final JokesRemoteDataSource remoteDataSource;
  final JokesLocalDataSource localDataSource;

  JokesRepositoryImpl(this.remoteDataSource, this.localDataSource);

  @override
  Future<Either<Failure, Joke>> getJokeForQuery(String query) async {
    return await _getJoke(() {
      return remoteDataSource.getJokeForQuery(query);
    });
  }

  @override
  Future<Either<Failure, Joke>> getRandomJoke() async {
    return await _getJoke(() {
      return remoteDataSource.getRandomJoke();
    });
  }

  Future<Either<Failure, Joke>> _getJoke(
    _QueryOrRandomChooser getQueryOrRandom,
  ) async {
    try {
      final Joke remoteJoke = await getQueryOrRandom();
      return Right<Failure, Joke>(remoteJoke);
    } catch (e) {
      getLogger().w('Error on retrieving joke from API: $e');
      return Left<Failure, Joke>(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, List<Joke>>> getFavorites() async {
    try {
      final List<JokeModel> favorites = await localDataSource.getFavorites();
      return Right<Failure, List<Joke>>(favorites);
    } catch (e) {
      getLogger().w('Error on retrieving favorites: $e');
      return Left<Failure, List<Joke>>(StorageFailure());
    }
  }

  @override
  Future<Either<Failure, List<Joke>>> addFavorite(Joke joke) async {
    try {
      final List<JokeModel> favorites = await localDataSource.addFavorite(joke as JokeModel);
      return Right<Failure, List<Joke>>(favorites);
    } catch (e) {
      getLogger().e('Error on adding favorite: $e');
      return Left<Failure, List<Joke>>(StorageFailure());
    }
  }

  @override
  Future<Either<Failure, List<Joke>>> removeFavorite(Joke joke) async {
    try {
      final List<JokeModel> favorites = await localDataSource.removeFavorite(joke as JokeModel);
      return Right<Failure, List<Joke>>(favorites);
    } catch (e) {
      getLogger().e('Error on removing favorite: $e');
      return Left<Failure, List<Joke>>(StorageFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> isFavorite(Joke joke) async {
    try {
      final bool isFavorite = await localDataSource.isFavorite(joke as JokeModel);
      return Right<Failure, bool>(isFavorite);
    } catch (e) {
      getLogger().e('Error on checking if joke is a favorite: $e');
      return Left<Failure, bool>(StorageFailure());
    }
  }
}
