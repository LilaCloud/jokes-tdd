import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:meta/meta.dart';

class JokeModel extends Joke {
  const JokeModel(
      {@required int id, @required bool isFavorite, @required bool isTwoPart, @required String text1, String text2})
      : super(id: id, isFavorite: isFavorite, isTwoPart: isTwoPart, text1: text1, text2: text2);

  factory JokeModel.fromJson(Map<String, dynamic> json) {
    return (json.containsKey('joke'))
        ? JokeModel(
            id: json['id'] as int ?? 0,
            isFavorite: json.containsKey('isFavorite') ? json['isFavorite'] as bool : false,
            isTwoPart: false,
            text1: json['joke'] as String ?? '')
        : JokeModel(
            id: json['id'] as int ?? 0,
            isFavorite: json.containsKey('isFavorite') ? json['isFavorite'] as bool : false,
            isTwoPart: true,
            text1: json['setup'] as String ?? '',
            text2: json['delivery'] as String ?? '');
  }

  Map<String, dynamic> toJson() {
    return isTwoPart
        ? <String, dynamic>{'id': id, 'isFavorite': true, 'setup': text1, 'delivery': text2}
        : <String, dynamic>{'id': id, 'isFavorite': true, 'joke': text1};
  }
}
