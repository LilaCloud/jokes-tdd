import 'dart:convert';

import 'package:jokes/core/util/logger.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class JokesLocalDataSource {
  Future<List<JokeModel>> getFavorites();

  Future<List<JokeModel>> addFavorite(JokeModel joke);

  Future<List<JokeModel>> removeFavorite(JokeModel joke);

  Future<bool> isFavorite(JokeModel joke);
}

const String SP_KEY_FAVORITES = 'SP_KEY_FAVORITES';

class JokesLocalDataSourceImpl implements JokesLocalDataSource {
  final SharedPreferences sharedPreferences;

  JokesLocalDataSourceImpl(this.sharedPreferences);

  @override
  Future<List<JokeModel>> getFavorites() async {
    try {
      return (jsonDecode(sharedPreferences.getString(SP_KEY_FAVORITES)) as List<dynamic>)
          .map((dynamic item) => JokeModel.fromJson(item as Map<String, dynamic>))
          .toList();
    } catch (e) {
      getLogger().w('Failed to parse favorites: $e');
      return <JokeModel>[];
    }
  }

  @override
  Future<bool> isFavorite(JokeModel joke) async {
    return (await getFavorites()).contains(joke);
  }

  @override
  Future<List<JokeModel>> addFavorite(JokeModel joke) async {
    final List<JokeModel> favorites = await getFavorites();
    if (!favorites.contains(joke)) {
      favorites.add(joke);
    }
    _persistFavorites(favorites);
    return favorites;
  }

  @override
  Future<List<JokeModel>> removeFavorite(JokeModel joke) async {
    final List<JokeModel> favorites = await getFavorites();
    favorites.remove(joke);
    _persistFavorites(favorites);
    return favorites;
  }

  void _persistFavorites(List<JokeModel> favorites) {
    sharedPreferences.setString(SP_KEY_FAVORITES, jsonEncode(favorites));
  }
}
