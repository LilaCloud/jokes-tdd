import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:jokes/core/error/exceptions.dart';

import 'package:jokes/features/jokes/data/models/joke_model.dart';

abstract class JokesRemoteDataSource {
  /// Calls remote API.
  /// Throws a [ServerException] for all error codes.
  Future<JokeModel> getRandomJoke();

  /// Calls remote API.
  /// Throws a [ServerException] for all error cases.
  Future<JokeModel> getJokeForQuery(String query);
}

class JokesRemoteDataSourceImpl implements JokesRemoteDataSource {
  final http.Client client;

  JokesRemoteDataSourceImpl(this.client);

  @override
  Future<JokeModel> getJokeForQuery(String query) =>
      _getJokeFromUrl('https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw&contains=${(query ?? '').trim()}');

  @override
  Future<JokeModel> getRandomJoke() => _getJokeFromUrl('https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw');

  Future<JokeModel> _getJokeFromUrl(String url) async {
    final http.Response response = await client.get(url);

    if (response.statusCode == 200) {
      return JokeModel.fromJson(json.decode(response.body) as Map<String, dynamic>);
    } else {
      throw ServerException();
    }
  }
}
