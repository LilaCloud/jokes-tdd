import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:jokes/features/jokes/data/datasources/jokes_local_data_source.dart';
import 'package:jokes/features/jokes/data/datasources/jokes_remote_data_source.dart';
import 'package:jokes/features/jokes/data/repositories/jokes_repository_impl.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_add_favorite.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_favorites.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_joke_by_query.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_random_joke.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_remove_favorite.dart';
import 'package:jokes/features/jokes/presentation/bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

final GetIt sl = GetIt.instance;

Future<void> init() async {
  /// Features - Number Trivia
  /// Bloc
  sl.registerFactory(() => JokesBloc(
      getRandomJoke: sl(), getJokeForQuery: sl(), getFavorites: sl(), removeFavorite: sl(), addFavorite: sl()));

  /// Use cases
  sl.registerLazySingleton(() => UseCaseGetJokeForQuery(sl()));
  sl.registerLazySingleton(() => UseCaseGetRandomJoke(sl()));
  sl.registerLazySingleton(() => UseCaseGetFavorites(sl()));
  sl.registerLazySingleton(() => UseCaseAddFavorite(sl()));
  sl.registerLazySingleton(() => UseCaseRemoveFavorite(sl()));

  /// Repository
  sl.registerLazySingleton<JokesRepository>(() => JokesRepositoryImpl(sl(), sl()));

  /// Data sources
  sl.registerLazySingleton<JokesRemoteDataSource>(() => JokesRemoteDataSourceImpl(sl()));
  sl.registerLazySingleton<JokesLocalDataSource>(() => JokesLocalDataSourceImpl(sl()));

  /// External
  final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
}
