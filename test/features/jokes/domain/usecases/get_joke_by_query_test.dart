import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_joke_by_query.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockJokesRepository extends Mock implements JokesRepository {}

void main() {
  UseCaseGetJokeForQuery useCase;
  MockJokesRepository mockJokesRepository;

  setUp(() {
    mockJokesRepository = MockJokesRepository();
    useCase = UseCaseGetJokeForQuery(mockJokesRepository);
  });

  const String tQuery = 'test';
  const Joke tJoke = JokeModel(id: 1, isFavorite: true, isTwoPart: true, text1: '1', text2: '2');

  test(
    'should get a joke for the given query from the repository',
    () async {
      // arrange
      when(mockJokesRepository.getJokeForQuery(any)).thenAnswer((_) async => Right<Failure, Joke>(tJoke));
      // act
      final Either<Failure, Joke> result = await useCase(const GetJokeForQueryParams(query: tQuery));
      // assert
      expect(result, Right<Failure, Joke>(tJoke));
      verify(mockJokesRepository.getJokeForQuery(tQuery));
      verifyNoMoreInteractions(mockJokesRepository);
    },
  );
}
