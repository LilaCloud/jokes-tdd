import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_remove_favorite.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockJokesRepository extends Mock implements JokesRepository {}

void main() {
  UseCaseRemoveFavorite useCase;
  MockJokesRepository mockJokesRepository;

  setUp(() {
    mockJokesRepository = MockJokesRepository();
    useCase = UseCaseRemoveFavorite(mockJokesRepository);
  });
  //
  // const List<Joke> tFavoritesBefore = <Joke>[
  //   Joke(id: 1, isTwoPart: true, text1: '1', text2: '2'),
  //   Joke(id: 2, isTwoPart: true, text1: '1', text2: '2')
  // ];
  const Joke tJokeToRemove = JokeModel(id: 2,isFavorite: true, isTwoPart: true, text1: '1', text2: '2');
  const List<Joke> tFavoritesAfter = <Joke>[JokeModel(id: 1,isFavorite: true, isTwoPart: true, text1: '1', text2: '2')];

  test(
    'should remove the joke from favorite jokes and return new favorites from the repository',
    () async {
      // arrange
      when(mockJokesRepository.removeFavorite(any)).thenAnswer((_) async => Right<Failure, List<Joke>>(tFavoritesAfter));
      // act
      final Either<Failure, List<Joke>> result = await useCase(const RemoveFavoriteParams(joke: tJokeToRemove));
      // assert
      expect(result, Right<Failure, List<Joke>>(tFavoritesAfter));
      verify(mockJokesRepository.removeFavorite(tJokeToRemove));
      verifyNoMoreInteractions(mockJokesRepository);
    },
  );
}
