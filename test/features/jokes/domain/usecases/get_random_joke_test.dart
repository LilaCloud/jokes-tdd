import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_random_joke.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockJokesRepository extends Mock implements JokesRepository {}

void main() {
  UseCaseGetRandomJoke useCase;
  MockJokesRepository mockJokesRepository;

  setUp(() {
    mockJokesRepository = MockJokesRepository();
    useCase = UseCaseGetRandomJoke(mockJokesRepository);
  });

  const Joke tJoke = JokeModel(id: 1, isFavorite: true, isTwoPart: true, text1: '1', text2: '2');

  test(
    'should get a joke from the repository',
    () async {
      // arrange
      when(mockJokesRepository.getRandomJoke()).thenAnswer((_) async => Right<Failure, Joke>(tJoke));
      // act
      final Either<Failure, Joke> result = await useCase(NoParams());
      // assert
      expect(result, Right<Failure, Joke>(tJoke));
      verify(mockJokesRepository.getRandomJoke());
      verifyNoMoreInteractions(mockJokesRepository);
    },
  );
}
