import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/repositories/jokes_repository.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_favorites.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockJokesRepository extends Mock implements JokesRepository {}

void main() {
  UseCaseGetFavorites useCase;
  MockJokesRepository mockJokesRepository;

  setUp(() {
    mockJokesRepository = MockJokesRepository();
    useCase = UseCaseGetFavorites(mockJokesRepository);
  });

  const List<Joke> tFavorites = <Joke>[JokeModel(id: 1, isFavorite: true, isTwoPart: true, text1: '1', text2: '2'), JokeModel(id: 2, isFavorite: true, isTwoPart: true, text1: '1', text2: '2')];

  test(
    'should get favorite jokes from the repository',
    () async {
      // arrange
      when(mockJokesRepository.getFavorites()).thenAnswer((_) async => Right<Failure, List<Joke>>(tFavorites));
      // act
      final Either<Failure, List<Joke>> result = await useCase(NoParams());
      // assert
      expect(result, Right<Failure, List<Joke>>(tFavorites));
      verify(mockJokesRepository.getFavorites());
      verifyNoMoreInteractions(mockJokesRepository);
    },
  );
}
