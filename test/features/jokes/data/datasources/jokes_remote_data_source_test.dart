import 'dart:convert';

import 'package:jokes/core/error/exceptions.dart';
import 'package:jokes/features/jokes/data/datasources/jokes_remote_data_source.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:matcher/matcher.dart';
import 'package:http/http.dart' as http;

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  JokesRemoteDataSource dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = JokesRemoteDataSourceImpl(mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.get(any)).thenAnswer((_) async => http.Response(fixture('joke_single.json'), 200));
  }

  void setUpMockHttpClientFailure404() {
    when(mockHttpClient.get(any)).thenAnswer((_) async => http.Response('Something went wrong', 404));
  }

  group('getJokeForQuery', () {
    const String tQuery = 'test';
    final JokeModel tJokeModel = JokeModel.fromJson(json.decode(fixture('joke_single.json')) as Map<String, dynamic>);

    test(
      'should perform a GET request on a URL with query',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        dataSource.getJokeForQuery(tQuery);
        // assert
        verify(mockHttpClient.get('https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw&contains=$tQuery'));
      },
    );

    test(
      'should return Joke when the response code is 200 (success)',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final JokeModel result = await dataSource.getJokeForQuery(tQuery);
        // assert
        expect(result, equals(tJokeModel));
      },
    );

    test(
      'should throw a ServerException when the response code is 404 or other',
      () async {
        // arrange
        setUpMockHttpClientFailure404();
        // act
        final Future<JokeModel> Function(String query) call = dataSource.getJokeForQuery;
        // assert
        expect(() => call(tQuery), throwsA(const TypeMatcher<ServerException>()));
      },
    );
  });

  group('getRandomJoke', () {
    final JokeModel tJokeModel = JokeModel.fromJson(json.decode(fixture('joke_single.json')) as Map<String, dynamic>);

    test(
      'should perform a GET request on a URL with query',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        dataSource.getRandomJoke();
        // assert
        verify(mockHttpClient.get('https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw'));
      },
    );

    test(
      'should return NumberTrivia when the response code is 200 (success)',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final JokeModel result = await dataSource.getRandomJoke();
        // assert
        expect(result, equals(tJokeModel));
      },
    );

    test(
      'should throw a ServerException when the response code is 404 or other',
      () async {
        // arrange
        setUpMockHttpClientFailure404();
        // act
        final Future<JokeModel> call = dataSource.getRandomJoke();
        // assert
        expect(() => call, throwsA(const TypeMatcher<ServerException>()));
      },
    );
  });
}
