import 'dart:convert';

import 'package:jokes/features/jokes/data/datasources/jokes_local_data_source.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:matcher/matcher.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  JokesLocalDataSource dataSource;
  MockSharedPreferences mockSharedPreferences;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    dataSource = JokesLocalDataSourceImpl(mockSharedPreferences);
  });

  group('favorites', () {
    const List<JokeModel> tFavoritesEmpty = <JokeModel>[];
    const List<JokeModel> tFavoritesOne = <JokeModel>[
      JokeModel(id: 1, isTwoPart: false, text1: 'test', isFavorite: true),
    ];
    const JokeModel tJoke = JokeModel(id: 1, isTwoPart: false, text1: 'test', isFavorite: true);
    const List<JokeModel> tFavoritesAsInFixture = <JokeModel>[
      JokeModel(id: 1, isTwoPart: false, text1: 'test', isFavorite: true),
      JokeModel(id: 2, isTwoPart: true, text1: 'test1', text2: 'test2', isFavorite: true)
    ];

    test(
      'should return empty list, when there is no data stored locally',
      () async {
        // arrange
        when(mockSharedPreferences.getString(any)).thenReturn('');
        // act
        final List<JokeModel> result = await dataSource.getFavorites();
        // assert
        verify(mockSharedPreferences.getString(SP_KEY_FAVORITES));
        expect(result, equals(tFavoritesEmpty));
      },
    );

    test(
      'should return favorites, when there is data stored locally',
      () async {
        // arrange
        when(mockSharedPreferences.getString(any)).thenReturn(fixture('favorite_jokes.json'));
        // act
        final List<JokeModel> result = await dataSource.getFavorites();
        // assert
        verify(mockSharedPreferences.getString(SP_KEY_FAVORITES));
        expect(result, equals(tFavoritesAsInFixture));
      },
    );

    test('should add new favorite and store updated favorites to shared preferences, when add is called', () async {
      // arrange
      // act
      final List<JokeModel> result = await dataSource.addFavorite(tJoke);
      // assert
      verify(mockSharedPreferences.setString(SP_KEY_FAVORITES, jsonEncode(<JokeModel>[tJoke])));
      expect(result, equals(tFavoritesOne));
    });

    test('should remove favorite and store updated favorites to shared preferences, when remove is called', () async {
      // arrange
      // act
      final List<JokeModel> result = await dataSource.removeFavorite(tJoke);
      // assert
      verify(mockSharedPreferences.setString(SP_KEY_FAVORITES, jsonEncode(<JokeModel>[])));
      expect(result, equals(tFavoritesEmpty));
    });
  });
}
