import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/exceptions.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/features/jokes/data/datasources/jokes_local_data_source.dart';
import 'package:jokes/features/jokes/data/datasources/jokes_remote_data_source.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/data/repositories/jokes_repository_impl.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockRemoteDataSource extends Mock implements JokesRemoteDataSource {}

class MockLocalDataSource extends Mock implements JokesLocalDataSource {}

void main() {
  JokesRepositoryImpl repository;
  MockRemoteDataSource mockRemoteDataSource;
  MockLocalDataSource mockLocalDataSource;

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockLocalDataSource = MockLocalDataSource();
    repository = JokesRepositoryImpl(mockRemoteDataSource, mockLocalDataSource);
  });

  group('getJokeByQuery', () {
    const String tQuery = 'test';
    const JokeModel tJokeModel = JokeModel(id: 1, isTwoPart: false, text1: 'test', isFavorite: false);
    const Joke tJoke = tJokeModel;

    test(
      'should return remote data when the call to remote data source is successful',
      () async {
        // arrange
        when(mockRemoteDataSource.getJokeForQuery(any)).thenAnswer((_) async => tJokeModel);
        // act
        final Either<Failure, Joke> result = await repository.getJokeForQuery(tQuery);
        // assert
        verify(mockRemoteDataSource.getJokeForQuery(tQuery));
        expect(result, equals(Right<Failure, Joke>(tJoke)));
      },
    );

    test(
      'should return server failure when the call to remote data source is unsuccessful',
      () async {
        // arrange
        when(mockRemoteDataSource.getJokeForQuery(any)).thenThrow(ServerException());
        // act
        final Either<Failure, Joke> result = await repository.getJokeForQuery(tQuery);
        // assert
        verify(mockRemoteDataSource.getJokeForQuery(tQuery));
        expect(result, equals(Left<Failure, Joke>(ServerFailure())));
      },
    );
  });

  group('getRandomJoke', () {
    const JokeModel tJokeModel = JokeModel(id: 1, isTwoPart: false, text1: 'test', isFavorite: false);
    const Joke tJoke = tJokeModel;

    test(
      'should return remote data when the call to remote data source is successful',
      () async {
        // arrange
        when(mockRemoteDataSource.getRandomJoke()).thenAnswer((_) async => tJokeModel);
        // act
        final Either<Failure, Joke> result = await repository.getRandomJoke();
        // assert
        verify(mockRemoteDataSource.getRandomJoke());
        expect(result, equals(Right<Failure, Joke>(tJoke)));
      },
    );

    test(
      'should return server failure when the call to remote data source is unsuccessful',
      () async {
        // arrange
        when(mockRemoteDataSource.getRandomJoke()).thenThrow(ServerException());
        // act
        final Either<Failure, Joke> result = await repository.getRandomJoke();
        // assert
        verify(mockRemoteDataSource.getRandomJoke());
        expect(result, equals(Left<Failure, Joke>(ServerFailure())));
      },
    );
  });

  group('Favorites', () {
    const List<JokeModel> tFavoritesBefore = <JokeModel>[JokeModel(id: 1, isTwoPart: true, text1: '1', text2: '2', isFavorite: true)];
    const JokeModel tNewJoke = JokeModel(id: 2, isTwoPart: true, text1: '1', text2: '2', isFavorite: true);
    const List<JokeModel> tFavoritesAfter = <JokeModel>[
      JokeModel(id: 1, isTwoPart: true, text1: '1', text2: '2', isFavorite: true),
      JokeModel(id: 2, isTwoPart: true, text1: '1', text2: '2', isFavorite: true)
    ];

    test('should return favorites from local data source.', () async {
      // arrange
      when(mockLocalDataSource.getFavorites()).thenAnswer((_) async => tFavoritesBefore);
      // act
      final Either<Failure, List<Joke>> result = await repository.getFavorites();
      // assert
      verify(mockLocalDataSource.getFavorites());
      expect(result, equals(Right<Failure, List<Joke>>(tFavoritesBefore)));
    });

    test('should update and return favorites using local data source, when a new joke is added.', () async {
      // arrange
      when(mockLocalDataSource.addFavorite(any)).thenAnswer((_) async => tFavoritesAfter);
      // act
      final Either<Failure, List<Joke>> result = await repository.addFavorite(tNewJoke);
      // assert
      verify(mockLocalDataSource.addFavorite(tNewJoke));
      expect(result, equals(Right<Failure, List<Joke>>(tFavoritesAfter)));
    });

    test('should update and return favorites using local data source, when a joke is removed.', () async {
      // arrange
      when(mockLocalDataSource.removeFavorite(any)).thenAnswer((_) async => tFavoritesBefore);
      // act
      final Either<Failure, List<Joke>> result = await repository.removeFavorite(tNewJoke);
      // assert
      verify(mockLocalDataSource.removeFavorite(tNewJoke));
      expect(result, equals(Right<Failure, List<Joke>>(tFavoritesBefore)));
    });
  });
}
