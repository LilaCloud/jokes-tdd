import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  const JokeModel tJokeModelSingle = JokeModel(id: 1, isTwoPart: false, text1: 'test', isFavorite: false);
  const JokeModel tJokeModelTwoPart = JokeModel(id: 1, isTwoPart: true, text1: 'test1', text2: 'test2', isFavorite: false);

  test(
    'should be a subclass of Joke entity',
    () async {
      // assert
      expect(tJokeModelSingle, isA<Joke>());
      expect(tJokeModelTwoPart, isA<Joke>());
    },
  );

  group('fromJson', () {
    test(
      'should return a valid model for single part jokes from api',
      () async {
        // arrange
        final Map<String, dynamic> jsonMap = json.decode(fixture('joke_single.json')) as Map<String, dynamic>;
        // act
        final JokeModel result = JokeModel.fromJson(jsonMap);
        // assert
        expect(result, tJokeModelSingle);
      },
    );

    test(
      'should return a valid model for two part jokes from api',
      () async {
        // arrange
        final Map<String, dynamic> jsonMap = json.decode(fixture('joke_twopart.json')) as Map<String, dynamic>;
        // act
        final JokeModel result = JokeModel.fromJson(jsonMap);
        // assert
        expect(result, tJokeModelTwoPart);
      },
    );
  });

  group('toJson', () {
    test(
      'should return a JSON map containing the proper data for single jokes',
      () async {
        // act
        final Map<String, dynamic> result = tJokeModelSingle.toJson();
        // assert
        final Map<String, Object> expectedJsonMap = <String, Object>{'id': 1, 'isFavorite': true, 'joke': 'test'};
        expect(result, expectedJsonMap);
      },
    );

    test(
      'should return a JSON map containing the proper data for two part jokes',
      () async {
        // act
        final Map<String, dynamic> result = tJokeModelTwoPart.toJson();
        // assert
        final Map<String, Object> expectedJsonMap = <String, Object>{'id': 1, 'isFavorite': true, 'setup': 'test1', 'delivery': 'test2'};
        expect(result, expectedJsonMap);
      },
    );
  });
}
