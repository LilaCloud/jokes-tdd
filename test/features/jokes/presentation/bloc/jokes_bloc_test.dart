import 'package:dartz/dartz.dart';
import 'package:jokes/core/error/failures.dart';
import 'package:jokes/core/usecases/usecase.dart';
import 'package:jokes/features/jokes/data/models/joke_model.dart';
import 'package:jokes/features/jokes/domain/entities/joke.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_add_favorite.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_favorites.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_joke_by_query.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_get_random_joke.dart';
import 'package:jokes/features/jokes/domain/usecases/use_case_remove_favorite.dart';
import 'package:jokes/features/jokes/presentation/bloc/bloc.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockUseCaseGetJokeForQuery extends Mock implements UseCaseGetJokeForQuery {}

class MockUseCaseGetRandomJoke extends Mock implements UseCaseGetRandomJoke {}

class MockUseCaseGetFavorites extends Mock implements UseCaseGetFavorites {}

class MockUseCaseAddFavorite extends Mock implements UseCaseAddFavorite {}

class MockUseCaseRemoveFavorite extends Mock implements UseCaseRemoveFavorite {}

void main() {
  JokesBloc bloc;
  MockUseCaseGetJokeForQuery mockUseCaseGetJokeForQuery;
  MockUseCaseGetRandomJoke mockUseCaseGetRandomJoke;
  MockUseCaseGetFavorites mockUseCaseGetFavorites;
  MockUseCaseAddFavorite mockUseCaseAddFavorite;
  MockUseCaseRemoveFavorite mockUseCaseRemoveFavorite;
  const String tQuery = 'test';
  const Joke tJoke = JokeModel(id: 1, isFavorite: true, isTwoPart: false, text1: 'test');

  setUp(() {
    mockUseCaseGetJokeForQuery = MockUseCaseGetJokeForQuery();
    mockUseCaseGetRandomJoke = MockUseCaseGetRandomJoke();
    bloc = JokesBloc(
        getJokeForQuery: mockUseCaseGetJokeForQuery,
        getRandomJoke: mockUseCaseGetRandomJoke,
        getFavorites: mockUseCaseGetFavorites,
        addFavorite: mockUseCaseAddFavorite,
        removeFavorite: mockUseCaseRemoveFavorite);
  });

  test('initialState should be Empty', () {
    // assert
    expect(bloc.initialState, equals(JokesStateEmpty()));
  });

  test(
    'should emit Empty, Loading, LoadedJoke automatically when bloc is created and data is gotten successfully for the random case',
    () async {
      // arrange
      when(mockUseCaseGetRandomJoke(any)).thenAnswer((_) async => Right<Failure, Joke>(tJoke));
      final JokesBloc newInstance = JokesBloc(
          getJokeForQuery: mockUseCaseGetJokeForQuery,
          getRandomJoke: mockUseCaseGetRandomJoke,
          getFavorites: mockUseCaseGetFavorites,
          addFavorite: mockUseCaseAddFavorite,
          removeFavorite: mockUseCaseRemoveFavorite);
      // assert later
      final List<JokesState> expected = <JokesState>[
        JokesStateEmpty(),
        JokesStateLoading(),
        JokesStateLoaded(tJoke, null)
      ];
      // could be a race issue here, because the bloc is created before we start listening... But it's working for now.
      expectLater(newInstance, emitsInOrder(expected));
      // act
      // nothing to do here, because the data should be loaded automatically when creating a new bloc instance
    },
  );

  group('GetJokeForQuery', () {
    test(
      'should get data from the query use case',
      () async {
        // arrange
        when(mockUseCaseGetJokeForQuery(any)).thenAnswer((_) async => Right<Failure, Joke>(tJoke));
        // act
        bloc.add(JokesEventGetJokeForQuery(tQuery));
        await untilCalled(mockUseCaseGetJokeForQuery(any));
        // assert
        verify(mockUseCaseGetJokeForQuery(const GetJokeForQueryParams(query: tQuery)));
      },
    );

    test(
      'should emit [Loading, Loaded] when data is gotten successfully',
      () async {
        // arrange
        when(mockUseCaseGetJokeForQuery(any)).thenAnswer((_) async => Right<Failure, Joke>(tJoke));
        // assert later
        final List<JokesState> expected = <JokesState>[
          JokesStateEmpty(),
          JokesStateLoading(),
          JokesStateLoaded(tJoke, null),
        ];
        expectLater(bloc, emitsInOrder(expected));
        // act
        bloc.add(JokesEventGetJokeForQuery(tQuery));
      },
    );

    test(
      'should emit [Loading, Error] when getting data fails',
      () async {
        // arrange
        when(mockUseCaseGetJokeForQuery(any)).thenAnswer((_) async => Left<Failure, Joke>(ServerFailure()));
        // assert later
        final List<JokesState> expected = <JokesState>[
          JokesStateEmpty(),
          JokesStateLoading(),
          JokesStateError(message: SERVER_FAILURE_MESSAGE),
        ];
        expectLater(bloc, emitsInOrder(expected));
        // act
        bloc.add(JokesEventGetJokeForQuery(tQuery));
      },
    );
  });

  group('GetRandomJoke', () {
    test(
      'should get data from the random use case',
      () async {
        // arrange
        when(mockUseCaseGetRandomJoke(any)).thenAnswer((_) async => Right<Failure, Joke>(tJoke));
        // act
        bloc.add(JokesEventGetRandomJoke());
        await untilCalled(mockUseCaseGetRandomJoke(any));
        // assert
        verify(mockUseCaseGetRandomJoke(NoParams()));
      },
    );

    test(
      'should emit [Loading, Loaded] when data is gotten successfully',
      () async {
        // arrange
        when(mockUseCaseGetRandomJoke(any)).thenAnswer((_) async => Right<Failure, Joke>(tJoke));
        // assert later
        final List<JokesState> expected = <JokesState>[
          JokesStateEmpty(),
          JokesStateLoading(),
          JokesStateLoaded(tJoke, null),
        ];
        expectLater(bloc, emitsInOrder(expected));
        // act
        bloc.add(JokesEventGetRandomJoke());
      },
    );

    test(
      'should emit [Loading, Error] when getting data fails',
      () async {
        // arrange
        when(mockUseCaseGetRandomJoke(any)).thenAnswer((_) async => Left<Failure, Joke>(ServerFailure()));
        // assert later
        final List<JokesState> expected = <JokesState>[
          JokesStateEmpty(),
          JokesStateLoading(),
          JokesStateError(message: SERVER_FAILURE_MESSAGE),
        ];
        expectLater(bloc, emitsInOrder(expected));
        // act
        bloc.add(JokesEventGetRandomJoke());
      },
    );
  });
}
